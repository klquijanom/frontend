import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';


class Creador extends React.Component{

    enviarForm(valores, acciones){

        console.log(valores);
        let datos = {
            id: valores.id,
            nombre: valores.valor
        }
        
        fetch(
            'http://localhost:4000/insertar',
            {          
                method:'POST',
                mode: "no-cors",
                headers: {  'Content-type':'application/json'},
                body: JSON.stringify(datos)
            }
        );
    };
    
    render(){
        let elemento=<Formik 
            initialValues={
                {
                    id:'',
                    valor:''

                }
            }
            onSubmit={this.enviarForm}

            validationSchema={ Yup.object().shape(
                {
                    id: Yup.number().typeError('Debe ser un número').required('Obligatorio'),
                    valor: Yup.string().required('Campo es obligatorio')

                }
            )

            }

            >
                <Container className="p-3">
                    <Form>
                        <div className="form-group">
                            <label htmlFor='id'>ID</label>
                            <Field name="id" type="text" className="form-control"/>
                            <ErrorMessage name="id" className="invalid-feedback"></ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor='valor'>Valor</label>
                            <Field name="valor" type="text" className="form-control"/>
                            <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                        </div>
                        <div className="form-group">
                            <Button type="submit" className="btn btn-primary m-4">Aceptar</Button>
                            <Button type="reset" className="btn btn-secondary">Cancelar</Button>
                        </div>
                    </Form>
                </Container>
            </Formik>;
            return elemento;
    };
}

export default Creador;